import 'dart:html';
import 'dart:svg';
import 'dart:js';

tickCallback(force, nodes, links, event) {
  List newNodes = force.callMethod("nodes", []);
  print ("tick");
  
  // TODO(jlewi): Should we be using D3 selectors to update
  // the elements rather than doing it manually?
  for (int i = 0; i < newNodes.length; i++) {    
    // Reposition the nodes.
    Element circle = nodes[i.toString()]["circle"];
    var x = newNodes[i]["x"].toString();
    var y = circle.attributes["cy"] = newNodes[i]["y"].toString();
    print("i: $i x: $x y: $y");
    circle.attributes["cx"] = newNodes[i]["x"].toString();
    circle.attributes["cy"] = newNodes[i]["y"].toString();
  }
  
  for (int i = 0; i < links.length; i++) {
    Map link = links[i];
    Element line = link["line"];
    JsObject source = newNodes[links.elementAt(i)["source"]];
    JsObject target = newNodes[links.elementAt(i)["target"]];
    line.attributes["x1"] = source["x"].toString();
    line.attributes["y1"] = source["y"].toString();
    
    line.attributes["x2"] = target["x"].toString();
    line.attributes["y2"] = target["y"].toString();    
  }
}

void main() {
  Map nodes = {
    "0": {
      "index": 0,
      "x": 200,
      "y": 200,
    },
    "1": {
      "index": 1,
      "x": 200,
      "y": 200,
    },
    "2": {
      "index": 2,
      "x": 200,
      "y": 200,
    }
  };

  List links = [{
      "source": "0",
      "target": "1"
    }, {
      "source": "1",
      "target": "2"
    },{
      "source": "2",
      "target": "0"
    }];

  String width = "400";
  String height = "400";

  Element svg = new SvgElement.tag("svg");

  querySelector("body").append(svg);
  svg.attributes["width"] = width;
  svg.attributes["height"] = height;

  nodes.forEach((key, node) {
    // Draw a circle for the node.
    Element circle = new SvgElement.tag("circle");
    svg.append(circle);
    circle.classes.add("node");
    circle.attributes["cx"] = node["x"].toString();
    circle.attributes["cy"] = node["y"].toString();
    circle.attributes["r"] = 8.toString();
    
    node["circle"] = circle;
  });
  
  // Draw the links.
  for (int i = 0; i < links.length; i++) {
    Element link = new SvgElement.tag("line");
    svg.append(link);
    Map source = nodes[links.elementAt(i)["source"]];
    Map target = nodes[links.elementAt(i)["target"]];
    link.attributes["x1"] = source["x"].toString();
    link.attributes["y1"] = source["y"].toString();
    
    link.attributes["x2"] = target["x"].toString();
    link.attributes["y2"] = target["y"].toString();
    
    link.classes.add("link");
    links[i]["line"] = link;
  }
  
  // Setup a D3 force layout to layout the graph.
  JsObject d3 = context["d3"];
  JsObject force = d3["layout"].callMethod("force", []);
  
  JsObject jsNodes = new JsObject.jsify(nodes.values);
  JsObject jsLinks = new JsObject.jsify(links);   
  JsArray jsSize = new JsArray();
  jsSize.add(width);
  jsSize.add(height);
  // Attach data to the nodes.
  force.callMethod("nodes", [jsNodes]);
  force.callMethod("links", [jsLinks]);
  force.callMethod("size", [jsSize]);
  
  JsArray original = force.callMethod("nodes", []); 
  force.callMethod("start", []);
    
  // Setup a callback to update the graph on each tick.
  force.callMethod("on", ["tick", (event) {
    tickCallback(force, nodes, links, event);}]);
}

void reverseText(MouseEvent event) {
  var text = querySelector("#sample_text_id").text;
  var buffer = new StringBuffer();
  for (int i = text.length - 1; i >= 0; i--) {
    buffer.write(text[i]);
  }
  querySelector("#sample_text_id").text = buffer.toString();
}
