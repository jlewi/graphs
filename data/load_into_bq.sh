#!/bin/bash
#
# A simple script for loading some fake data into bigquery.
while getopts ?c:? OPTION
do
  case $OPTION in
      c)
        COMMAND=$OPTARG
          ;;
      ?)
          echo "You must use -c to specify the command."
          exit
          ;;
  esac
done

BQIMPORT=~/git_cloud/scripts/import_to_bigquery.py
DATASET=graphs
BASEPATH=gs://biocloudops-temp/graphs
PROJECT=biocloudops

CopyToGcs() 
{
# Copy the data to gcs.
gsutil cp nodes.json ${BASEPATH}/nodes.json
gsutil cp edges.json ${BASEPATH}/edges.json
}

ImportNodes()
{
SCHEMA='[{"name":"node_id","type":"string"},{"name":"index","type":"integer"}]'
${BQIMPORT} \
    --dataset=${DATASET} \
    --path=${BASEPATH}/nodes.json \
    --table=nodes \
    --schema=${SCHEMA} \
    --project=${PROJECT}
}

ImportEdges()
{
SCHEMA='[{"name":"edge_id","type":"string"},{"name":"source","type":"string"}'
SCHEMA=${SCHEMA}',{"name":"target","type":"string"}'
SCHEMA=${SCHEMA}',{"name":"source_index","type":"integer"}'
SCHEMA=${SCHEMA}',{"name":"target_index","type":"integer"}'
SCHEMA=${SCHEMA}']'

${BQIMPORT} \
    --dataset=${DATASET} \
    --path=${BASEPATH}/edges.json \
    --table=edges \
    --schema=${SCHEMA} \
    --project=${PROJECT}
}

${COMMAND}